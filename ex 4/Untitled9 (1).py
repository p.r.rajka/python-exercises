#!/usr/bin/env python
# coding: utf-8

# # PYTHON EXERCISE SET 4 -- RAJKA PEJANOVIC
# 

# Exercise 4.1: Prepare a file with a function called fence such that given two strings as arguments: string_1 = "aaa" and string_2 = "bbb", the output is aaa_bbb_aaa. In the same file define a second function called outer such that given a string returns another string made up of just the first and last characters of its input. Therefore if the input is Betis the function output should be Bs. Include in both cases a docstring with a brief function description and an example. Load the functions from the file and check what is the output of this statement: print outer(fence('carbon', '+')).

# In[3]:


def fence (s1, s2):
    return s1 +"_" + s2 + "_" + s1
#so if we have for s1=R and s2=K function will return R_K_R
print (__doc__,)
print (fence('aaa','bbb'))
def outer(s):
    return s[0:1]+s[-1:] 
#this function do next: we have Rajka as an input so output should be Ra"
print (__doc__,)
print (outer('Betis'))
print ("Outer - outer(fence('carbon', '+')) is:", outer(fence('carbon', '+')))


# Exercise 4.2: Write a function that generates a random password. The password should have a random length of between 10 and 12 random characters from positions 33 to 122 in the ASCII table. Your function will not take any parameters and will return the password as its only result. Make another function that checks if the password has at least two lowcase, two uppercase, and two digit characters and check how many times you need to run the original function to obtain a compliant password.

# In[37]:


from random import randint
import re
import numpy as np
#password should have between 10 and 12 characters
Upperlimit=12
Lowerlimit=10
minASCII=33
maxASCII=122

def randompass():
    #random lenght
    randomlenght=randint(Lowerlimit, Upperlimit)
    res=""
    for i in range (randomlenght):
        randomChar=chr(randint(minASCII, maxASCII))
        res=res+randomChar
    return res #random password as a return 
    
print ("Password is:", randompass())
           #Function that check password
lower=0
upper=0
digit =0
if (len(randompass())>=10):
    for i in randompass():
        if (i.islower()):
           lower+=1 #count lower letters
        if (i.isupper()):
           upper+=1 #count upper characters
        if(i.isdigit()):
           digit+=1
    if(lower>=2 and upper>=2 and digit>=2):
        print("Valid password")
    else:
        print ("Password is not valid")
    


# Exercise 4.3: Gaussian distributed data are frequently normalized to have a mean value equal to zero and a standard deviation equal to one substracting the actual mean value and dividing by the standard deviation of the dataset. Making use of the mean and std NumPy methods, define a function that takes as an argument a data vector, a new mean value, and a new standard deviation value and transforms the original set of data to a new set with a the new mean as its average value and with a dispersion given by the new standard deviation value. By default the function should standardize the data to mean = 0 and sdev = 1.

# In[22]:


import numpy as np
def datagen (inp, mean=0, stdev=1):
    avg=inp.mean()
    std=inp.std()
    datagen=inp-avg
    datagen=datagen/std*stdev
    datagen=datagen+mean
    return datagen

n=20
v=np.random.randn(100)*n
v=v+50
n_mean=50
n_std=30
ndata=datagen(v, n_mean, n_std)

ndata.mean()


# In[23]:


ndata.std()


# Exercise 4.4: You can approximate the cubic root of a number a as x(n+1) = 2 x(n)\/3 + a\/(3 x(n)²) with x(0) = a\/3. Prepare a function that computes the cubic root of a given root until the difference between successive computed values is less than a given threshod (e.g. 1E-8) and compare the obtained value and the value of x0**(1/3).

# In[26]:


def cuberoot (a, b):
    x1=a/3
    x2=2*x1+a/(3*x1**2)
    while(abs(x2-x1)>b):
        xt=x2
        x2=2+x1/3+a/(3*x1**2)
        x1=xt
    return x2
a=125
print (cuberoot(a, 1e-8))
cuberoot(a, 1e-8)-a**(1/3)


# Exercise 4.6: The sieve of Erastosthenes is an ancient algorithm (approx. 3rd century BCE) to find all prime numbers up to a given limit. The implementation of this algorithm in pseudocode is as follows:
# algorithm Sieve of Eratosthenes is input: an integer n > 1. output: all prime numbers from 2 through n.
# let A be an array of Boolean values, indexed by integers 2 to n, initially all set to true. 
# for i = 2, 3, 4, ..., not exceeding n**0.5 do 
# if A[i] is true 
#  for j = i**2, i**2+i, i**2+2i, i**2+3i, ..., not exceeding n do 
#  A[j] := false 
# return all i such that A[i] is true.

# In[32]:



def SieveOfEratosthenes(n):
# Create a boolean arra "prime[0..n]" and initialize all entries it as true, 
#value in prime[i] will finally be false if i is not a prime, else true.
    prime = [True for i in range(n+1)]
    p = 2
    while (p * p <= n):
# If prime[p] is not changed, then it is a prime
        if (prime[p] == True):
# Update all multiples of p
            for i in range(p * p, n+1, p):
              prime[i] = False
        p += 1
# Print all prime numbers
    for p in range(2, n+1):
        if prime[p]:
            print(p)
if __name__ == '__main__':
    n = 50
    print("Following are the prime numbers smaller"),
    print("than or equal to", n)
    SieveOfEratosthenes(n)


# In[ ]:




