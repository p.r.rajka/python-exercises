#!/usr/bin/env python
# coding: utf-8

# # Exercises RAJKA PEJANOVIĆ
# Exercise (1.1):
# How would you compute minimum, maximum and average March temperatures for the given dataset?

# In[2]:


import numpy as np
data = np.loadtxt(fname ='./TData/T_Agrinio_EM.csv', delimiter=',', skiprows=1)

March=data[:,3]
Minimum=np.min(March)
Maxuimum=np.max(March)
Mean=np.mean(March)


# 
# Exercise (1.2): 
# Check the info on the numpy mean function and compute the average monthly temperatures and the average annual temperatures for the given dataset (Hint: axis option)

# In[3]:


monthly_average = np.mean(data[:,1:], axis=0)
yearly_average = np.mean(data[:,1:], axis=1)


# In[4]:


monthly_average


# In[4]:


yearly_average


# Exercise (1.3): 
# Get the maximum and minimum monthly temperatures for year 2000 and at what months did these temperatures occur.

# In[5]:


i=np.where(data==2000)[0][0]
year2000=data[i,1:]

mean2000=np.mean(year2000)
max2000=np.max(year2000)
min2000=np.min(year2000)


# In[6]:


print(mean2000)


# In[7]:


print(max2000)


# In[8]:


print(min2000) 

