#!/usr/bin/env python
# coding: utf-8

# # EXERCISE 3 RAJKA PEJANOVIĆ

# # Exercise 3.2: 
# Prepare a loop such that given when it is given as an input a string variable it produces as output another string variable equal to the first string in reversed order, e.g. a = "abcd" and reverseda = "dcba".

# In[1]:


str = input("My string is: ") #Insert your string
  
new_String = ""   # Declaring empty string to store the reversed string
count = len(str)  # Find length of a string and save in count variable

while count > 0:   
    new_String += str[ count - 1 ]  # save the value of str[count-1] in new_String  
    count = count - 1 # decrement index  
print ("New string is : ",new_String)


# # Exercise 3.3: 
# Use a loop to convert the string "Testing loops and strings…" into another string, changing spaces into "_" (underscore).

# In[7]:


string = input("Enter a string") #Inserting a string
NewString = ''.join(["_" if i == " " else i for i in string]) 
#define condition and using the join() method which takes all items in an iterable and joins them into one string
print(NewString) #Result


# # Exercise 3.4: 
# Given the list AA = [1.0, -2.0, 3.0, 5.5, 0.3] and considering that these are the values of the coefficients for a polynomial Px = AA[0] + AA[1]*x + ... + AA[n]*x**n, prepare a loop that computes the polynomial value for a given independent variable x value. For example, in the given case Px = 7.8 for x = 1

# In[3]:


poly=[1.0,-2.0, 3.0, 5.5, 0.3] #We define the coefficients of the polynomial using a list
Sum=0
x = eval(input("Enter x: ")) #Enter value of variable x
for i in range(len(poly)): #Condition 
    Sum+=poly[i]*x**i #Where first term in polynomial is defined as i=0 (2nd line of code)
print("The polynomial value for a given independent variable x value is : ", Sum) #Result


# # Exercise 3.5: 
# Compute for the loaded temperature dataset the average seasonal temperatures and gives as a results a Python list with these temperatures.
# 

# In[5]:


import numpy as np


# In[6]:


temp_data = np.loadtxt(fname='./TData/T_Alicante_EM.csv', delimiter=',', skiprows=1) #Using given dataset


# In[7]:


Winter = [12,1,2]
Spring = [3,4,5]
Summer = [6,7,8]
Autumn = [9,10,11]  #Defining the seasons


# In[8]:


Average=[np.mean(temp_data [:, Winter]) , 
        np.mean(temp_data [:, Spring]) , 
        np.mean(temp_data [:, Summer]) , 
        np.mean(temp_data [:, Autumn]) ] #This list contain average temperatures for each season 


# In[10]:


print ("Average seasonal temepratures are:" ,Average) #Result


# # Exercise 3.6: 
# Build a code that, for a given month and considering the loaded temperature dataset, finds the mean and minimum temperature values for the selected month and then it builds a Python list with the years that have an average temperature for the selected month that is less than the average value of the minimum and mean temperature.

# In[2]:


import numpy as np
import calendar
temp_data = np.loadtxt(fname='./TData/T_Alicante_EM.csv', delimiter=',', skiprows=1) #Using given dataset
month_dict = {month: index for index, month in enumerate(calendar.month_abbr) if month}
month = input("Enter any month (first three letters): ")
min = np.min(temp_data[:, month_dict[month]]) #defining min temperature for selected month
mean = np.mean(temp_data[:, month_dict[month]]) #defining mean temperature for selected month
years_list = [] #create a new list which will be the output
for i in temp_data[:, month_dict[month]]: 
    if i < ((min + mean)/2): #condition
        years_list.append(temp_data[np.where(temp_data[:,month_dict[month]] == i)][0][0])
print("The list of years with average temperature for", month_input,
"that is less than the average of the minimum and mean temperature is \n", years_list)  #result

