#!/usr/bin/env python
# coding: utf-8

# # PYTHON EXERCISES - SET 4 --- RAJKA PEJANOVIĆ

# 5.1
# The Caesar's cipher is a historically relevant way of encrypting messages,
# already used by Julius Caesar, that is a substitution cipher. Given an integer value m, any
# character is replaced by the character found shifting the original character by m positions
# in the alphabet. Prepare a function that encrypts or decrypts a given message (assuming we
# only use the 26 capital letters of the English alphaber and replacing other symbols that are
# not letters by themselves, e.g. spaces and punctuation symbols) using a given value of m.
# Hints:
# (1) You can transform a strin in uppercase characters using the upper() method (up_strin =
# orig_string.upper()).
# (2) You can obtain the English ascii uppercase letters using the module string (import
# string) with the command ABC = string.ascii_uppercase.

# In[10]:


def Caesar_c(msg,m): #string with all letters
    msg=msg.upper() #change into upper oletters
    letters='ABCDEFGHIJKLMNOPQRSTUVWXYZ' #define all letters we are going to use
    list=[]  #We create a new list for storage
    for i in msg:
        a=0
        for k in letters:
            if i==k:
                list.append(a)
            a+=1 #using loop we will compare letters from string with letters from alphabet, in our case "letters"
    encrypted='' #new string
    a=0        
    for i in msg:
        if i.isalpha()==True: #This will modify only the letters
            new_list=m+list[a]                    
            if new_list>25: # we dont want position bigger then 26 so we introduce this condition
                new_list=new_list-len(letters)    
            encrypted+=letters[new_list]           
            a+=1
        else:
            encrypted+=i
    
    return encrypted
# testing
msg = input("Enter your message: ")
m = int(input("Enter an integer m: "))
print("This is your message after encryption: ", Caesar_c(msg,m))


# Exercise 5.2: The NIST Digital Library of Mathematical Functions (DLMF) is a very useful
# site, where you can find an updated and expanded version of the well-known reference
# Handbook of Mathematical Functions compiled by Abramowitz and Stegun. Define a
# function to compute the Bessel function of the first kind of integer index from the series
# 10.2.2 in the DLMF, add a docscript and plot the functions of order 0, 1, and 2 in the interval
# of x between 0 and 10.

# In[83]:


import numpy as np
import scipy.special
import math
import matplotlib.pyplot as plt #import all we need
def Bessel_func_1st(v,z):
    J=0                                
    for k in range(0,20): #20 is limit between 0 and 10
        J+=(-1)**k * (z/2)**(2*k) / (math.factorial(k)*scipy.special.gamma(v+k+1))
    return (z/2)**v * J
x=np.linspace(0,10,50)
    
#We do a plot with all the 3 functions
plt.plot(x,Bessel_func_1st(0,x),x,Bessel_func_1st(1,x),x,Bessel_func_1st(2,x))
plt.title('Bessel f - 1st kind (NIST)')
plt.grid()
plt.legend(['1st','2nd','3rd'])
plt.show ()
for i in range(3):  #here we will have 3 plots separately for each order 0, 1 and 2
    order='Bessel f - 1st kind of order ' + str(i)
    plt.plot(x,Bessel_func_1st(i,x),linewidth=5)
    plt.plot(x,scipy.special.jv(i, x),'o')
    plt.title(order)
    plt.grid()
    plt.legend(['NIST','Python'])
    plt.show()


# Exercise 5.3: Define and test a function that estimates the value of the special constant pi
# by generating N pairs of random numbers in the interval -1 and 1 and checking how many
# of the generated number fall into a circumference of radius 1 centered in the origin.
# Improve the function showing in a graphical output the square, the circumference, and the
# points inside and outside the circumference with different colors.

# In[84]:


def estimate_pi(N):
    p, q = (2*np.random.rand(N) - 1, 2*np.random.rand(N) - 1)
    inside_c=0 
    inside_s = 0
    outside_cX =[]
    outside_cY =[]
    inside_cX = []
    inside_cY = [] #list in which we will store x,y inside and outside circle
    for i in range(0, N):
        inside_s += 1
        if np.sqrt(p[i]**2 + q[i]**2) <= 1:
            inside_c += 1
            inside_cX.append(p[i])
            inside_cY.append(q[i]) #find points inside circle and square
        else:
            outside_cX.append(p[i])
            outside_cY.append(q[i]) #points outside
            pi_value = 4*inside_c/inside_s #obtain value for pi
    print("The value of pi is estimated to be", pi_value)
        # plotting
    fig, ax = plt.subplots(figsize=(7,7))
    circle = plt.Circle((0, 0), 1, fill = False)
    ax.scatter(outside_cX, outside_cY, s=10)
    ax.scatter(inside_cX, inside_cY, s=10)
    ax.add_patch(circle)
    ax.margins(x=0, y=0)
    ax.set_aspect('equal')
    fig.show()
estimate_pi(500)
    


# Exercise 5.4: The aim of this exercise is to generate a set of two-dimensional random
# walks, plot their trajectories and look at the end point distribution. The random walks
# considered always begin at the origin and take Nstep random steps of unit or zero size in
# both directions in the x and y axis.
# For a total number of Nw walks:
# 1. Compute the trajectories and save the final point of all of them.
# 2. Plot a sample of these random walks in the plane.
# 3. Plot all the final points together.
# 4. Compute the average distance of the final points from the origin.
# 5. Plot a histogram with the values of the distance to the origin.

# In[85]:


import numpy as np
def pg1(N):#path generator 1
    x_ran=np.random.randint(-1,2,[N])
#x=1 moves to the right,  x=0 does nothing, x=-1 moves to the left, y=1 moves upwards,y =0 does nothing, y=-1 moves downwards
    y_ran=np.random.randint(-1,2,[N])#first position is (0,0)         
    x_path,y_path=[],[]
    step_x,step_y=0,0
    for i in range(N):
        x_path.append(step_x) #We add the random steps at x and to the list as well       
        y_path.append(step_y) #Here same just for y
        step_x=step_x+x_ran[i]
        step_y=step_y+y_ran[i]
    return x_path, y_path
def pg2(N): #path gen 2 (this one wont allowe us to go on pervious step)
    x_ran=np.random.randint(-1,2,[N])
    y_ran=np.random.randint(-1,2,[N])
    for i in range(N-1):
        if x_ran[i]==x_ran[i+1] and y_ran[i]==y_ran[i+1]:
            x_ran[i],y_ran[i]=np.random.randint(-1,2),np.random.randint(-1,2)
    x_path,y_path=[],[]   #first position is (0,0)  
    step_x,step_y=0,0
    for i in range(N):
        x_path.append(step_x) #We add the random steps to x and to the list
        y_path.append(step_y) #same for y
        step_x=step_x+x_ran[i]
        step_y=step_y+y_ran[i]
    return x_path, y_path
import matplotlib.pylab as plt
x1,y1=[],[] #storing final points 
#we use pg1 to generate 10 random walks
N=20 #steps
n=10 #walks
for i in range(n):
    xsteps=pg1(N)[1]
    ysteps=pg1(N)[1]
    x1.append(xsteps[-1])                  
    y1.append(ysteps[-1])                 
    plt.plot(xsteps,ysteps)#Plot of every walk
plt.title('Random walks 1')              
plt.plot(x1,y1,'p',color='r',label='Final points') #Plot of final points
plt.legend()
plt.show()
#We use pg2 to generate another 10 random walks of 20 steps
x2,y2=[],[]
for i in range(n):
    xsteps=pg2(N)[0]
    ysteps=pg2(N)[1]
    x2.append(xsteps[-1])                  
    y2.append(ysteps[-1])                
    plt.plot(xsteps,ysteps)             
plt.title('Random walks 2')              
plt.plot(x2,y2,'p',color='g',label='Final points')
plt.legend()
plt.show()
#For getting the average distance of the final points from the origin of each plot
d1,d2=[],[]
for i in range(n):
    d1.append(np.sqrt(x1[i]**2+y1[i]**2))
    d2.append(np.sqrt(x2[i]**2+y2[i]**2))
d11=np.round(np.mean(d1),2)
d22=np.round(np.mean(d2),2)
print('The average distance from the origin using the random pg1 is d =',d11)
print('The average distance from the origin using the random pg2 is d =',d22)
#The histograms with the distances are made using plt.bar
bar=[]
for i in range(n):
    bar.append(i+1)
plt.bar(bar,d1,width=0.5)
plt.title('Random path generator 1')
plt.xlabel('Walk')
plt.ylabel('Distance from origin')
plt.show()
plt.bar(bar,d2,width=0.5)
plt.title('Random path generator 2')
plt.xlabel('Walk')
plt.ylabel('Distance from origin')
plt.show()


# Exercise 5.5:
# The Julia set is an important concept in fractal theory. Given a complex number a, a point z in the complex plane is said to be in the filled-in Julia set of a function f(z) = z² + a if the iteration of the function over the point does not finish with the point going to infinity. It can be proved that, if at some iterate of a point under f(z) the result has a module larger than 2 and larger than the module of a, this point will finish going to infinity. Build and plot the filled-in Julia sets for f(z) with a = (-0.5,0),(0.25,-0.52), (-1,0), (-0.2, 0.66) in the interval of -1 < Re(z), Im(z) < 1 and consider that the point belongs to the set once the previous condition has not been accomplished after Niter = 100. Hint: You can make use of the NumPy meshgrid and the PyPlot pplot functions for displaying the filled-in Julia sets.

# In[1]:


import math
import cmath
import numpy as np
import matplotlib.pylab as plt
def Julia(z,a):
    N=0 #Number of iterations
    while N<100:                         
        if abs(z)>2 and abs(z)>abs(a):
            break                       
        z=z**2+a
        N+=1
    return N#The function returns the number of iterations
a=np.array([ [-0.5,0.25,-1,-0.2], [0,-0.52,0,0.66] ]) #defining matrix for plot
n=400 #400x400 matrix
z_r=np.linspace(-1,1,n)#we set Re(z)<|1|
z_im=np.linspace(-1,1,n) 
z=[]
for i in range(n):
    for j in range(n):
        z.append(complex(z_r[i],z_im[j]))
z=np.reshape(z,[n,n])
zmesh=np.vectorize(Julia) #vectorite function
for i in range(4):   #plot the 4 Julia sets: 
    c=a[0,i]+1j*a[1,i]
    zplot=zmesh(z,c)
    plt.imshow(zplot,interpolation='nearest',cmap='hot')
    plt.axis('off')
    plt.show()
    #Here, we plot again the four Julia sets but for |z|<2
n=400
z_r=np.linspace(-2,2,n)       
z_im=np.linspace(-2,2,n)   
z=[]
for i in range(n):
    for j in range(n):
        z.append(complex(z_r[i],z_im[j]))
z=np.reshape(z,[n,n])
zmesh=np.vectorize(Julia)
for i in range(4):    
    c=a[0,i]+1j*a[1,i]
    zplot=zmesh(z,c)
    plt.imshow(zplot,interpolation='nearest',cmap='hot')
    plt.axis('off')
    plt.show()


# In[ ]:




