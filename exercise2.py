#!/usr/bin/env python
# coding: utf-8

# # EXERCISE 2 - RAJKA PEJANOVIĆ

# I recommend running .ipynb file since the figures show inline where .py file fires a new windows for each figure

# In[81]:


import numpy as np
from matplotlib import pyplot as plt
data = np.genfromtxt(fname ='./TData/T_Antalya_EM.csv', delimiter=',', skip_header=1)


# Exercise 2.1: 
# Plot the monthly and annual difference between max and min temperatures as a function of the month (1-12) and the year (1961-2096), respectively. In this case try to combine the plt.plot and plt.scatter functions. Hint: the plot function accept the syntax plt.plot(x,y).

# In[82]:


plt.style.use('fivethirtyeight') 
plt.rcParams['figure.figsize'] = [16, 12] 


# In[83]:


diffX1=np.max(data[:,1:], axis=0) - np.min(data[:,1:], axis=0)


# In[84]:


diffX2=np.max(data[:,1:], axis=1) - np.min(data[:,1:], axis=1)


# In[85]:


plt.plot(range(1,13), diffX1)
plt.xlabel("Month")
plt.ylabel("Temp") 
plt.title("Plot of the monthly difference between max and min temperatures as a function of the month") #USING PLT.PLOT
plt.show()

# In[86]:


plt.scatter(range(1961, 2097), diffX2) 
plt.xlabel("Year")
plt.ylabel("Temp")
plt.title("Annual minimum-maximum temperature difference (plt.scatter)") #USING PLT.SCATTER
plt.show()

# In[87]:


plt.plot(range(1961, 2097), diffX2) 
plt.xlabel("Year")
plt.ylabel("Temp")
plt.title("Annual minimum-maximum temperature difference") #USING PLT.PLOT
plt.show()

# Exercise 2.2: 
# Plot the standard deviation of the monthly and annual temperatures as a function of the month (1-12) and the year (1961-2096), respectively. Hint: check the std function in NumPy.

# In[88]:


plt.plot(range(1,13), data[:,1:].std(axis=0))
plt.xlabel ("Month")
plt.ylabel("Temp")
plt.title("Plot of the standard deviation of the monthly temperatures as a function of the month") #PLT.PLOT
plt.show()

# In[89]:


plt.plot(range(1961,2097), data[:,1:].std(axis=1))
plt.xlabel ("Year")
plt.ylabel("Temp")
plt.title("Plot of the standard deviation of the yearly temperatures as a function of the year")
plt.show()

# In[90]:


plt.scatter(range(1961,2097), data[:,1:].std(axis=1))
plt.xlabel ("Year")
plt.ylabel("Temp")
plt.title("Plot of the standard deviation of the yearly temperatures as a function of the year (plt.scatter)") #PLT.SCATTER
plt.show()

# Exercise 2.3: 
# Prepare a plot with two panels (arranged as you wish) which depicts the annual dependence of the average Spring and Fall temperatures for meteorological seasons: Spring (Mar, Apr, May) and Fall (Sep, Oct, Nov).

# In[116]:


fig, ax=plt.subplots (2,3)
fig.tight_layout(pad=5.0)
fig.suptitle("Plot with two panels which depicts the annual dependence of the average Spring and Fall temperatures for meteorological seasons")

ax[0,0].plot(range(1961,2097), data[:,3])
ax[0,0].set_title("March")
ax[0,1].plot(range(1961,2097), data[:,4])
ax[0,1].set_title("April")
ax[0,2].plot(range(1961,2097), data[:,5])
ax[0,2].set_title("May")
ax[1,0].plot(range(1961,2097), data[:,9])
ax[1,0].set_title("September")
ax[1,1].plot(range(1961,2097), data[:,10])
ax[1,1].set_title("October")
ax[1,2].plot(range(1961,2097), data[:,11])
ax[1,2].set_title("November")
for x in ax.flat:
    x.set(xlabel='year', ylabel='Temperature')
    
for x in ax.flat:
    x.label_outer()
plt.show()

# In[ ]:




